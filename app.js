const Koa = require('koa');
const bodyParser = require('koa-bodyparser');
const koaStatic = require('koa-static');

const app = new Koa();

const logger = require('./logger');

// Routing:
const api = require('./routes/api');
const webClient = require('./routes/web-client');

app 
    .use(bodyParser())
    .use(koaStatic('./public'))
    .use(logger(__dirname + '/journal.log'))
    .use(api.routes())
    .use(webClient.routes());
    //.use(api.allowedMethods());

app.listen(3000);