document.addEventListener('DOMContentLoaded', () => {
    let xhr = new XMLHttpRequest();

    xhr.open('GET', 'http://localhost:3000/api/halls/1/sessions', true);
    xhr.send();

    xhr.addEventListener('readystatechange', () => {
        if (xhr.readyState !== 4) return;

        if (xhr.status !== 200) {
            console.error('Data not loaded');
            return;
        }
        
        let data = JSON.parse(xhr.response);

        data.forEach(entry => showData(entry));
    });

    function showData(entry) {
        let data = {
            time: entry.time.substring(11, 16),
            MPAA: entry.movie.MPAA,
            title: entry.movie.title,
            duration: entry.movie.duration,
            genre: (() => {
                let res = '';
    
                entry.movie.genre.forEach(genre => res += genre + ', ');
    
                return res.substring(0, res.length - 2);
            })(),
            country: entry.movie.country
        };

        let parentNode = document.querySelector('.table-rows');

        let row = document.createElement('tr');
        row.classList.add('content-row');

        for (let key in data) {
            let cell = document.createElement('td');
            cell.classList.add(key.toLowerCase());
            cell.innerText = data[key];
            row.appendChild(cell);
        }

        parentNode.appendChild(row);
    }
});