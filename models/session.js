const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Movie = require('./movie');

let SessionSchema = new Schema({
    hall: Number,
    time: Date,
    movie: Movie.schema
});

module.exports = mongoose.model('Session', SessionSchema)