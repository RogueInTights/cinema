const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let MovieSchema = new Schema({
    title: String,
    genre: [String],
    duration: Number,
    MPAA: String, 
    country: String
});

module.exports = mongoose.model('Movie', MovieSchema)