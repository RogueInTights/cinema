const fs = require('fs');

module.exports = path => {
    return async (ctx, next) => {
        const date = new Date();
        let day = date.getDay();
        let month = date.getMonth();
        let year = date.getFullYear();
        let hours = date.getHours();
        let minutes = date.getMinutes();
        
        if (('' + minutes).length < 2) minutes = '0' + minutes;

        let timestamp = `${day}.${month}.${year} ${hours}:${minutes}`;
        let output = `(${timestamp}) ${ctx.method}: ${ctx.url}`;

        // Output:
        console.log(output);
        fs.appendFile(path, output + '\n', 'utf-8', err => {
            if (err) console.error(err)
        });
    
        await next();
    }
}