const Router = require('koa-router');
const fs = require('fs');

const router = new Router();

router
    .get('/', async ctx => {
        ctx.set('Content-Type', 'text/html');

        await new Promise((resolve, reject) => {
            fs.readFile('./views/index.html', 'utf-8', (err, data) => {
                if (err) {
                    ctx.status = 500;
                    console.error(err);
                }
                
                resolve(data);
            })
        }).then(data => {
            ctx.body = data;
        });
    });


module.exports = router