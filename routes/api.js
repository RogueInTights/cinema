const Router = require('koa-router');
const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

// Mongoose models:
const Movie = require('../models/movie');
const Session = require('../models/session');

mongoose.connect('mongodb://localhost/cinema', {useMongoClient: true});

const router = new Router();
const moviesRouter = new Router();
const sessionsRouter = new Router();

router.get('/api', ctx => {
    let package = require('../package.json');

    ctx.set('Content-Type', 'text/plain');
    ctx.body = `Cinema API. Version: ${package.version}`;
});

moviesRouter
    .post('/', async ctx => {
        let body = ctx.request.body;

        // TODO #1: Validation

        let movie = new Movie({
            title: body.title,
            genre: body.genre,
            duration: body.duration,
            MPAA: body.MPAA,
            country: body.country
        });
    
        await movie.save(err => {
            if (err) {
                ctx.status = 500;
                return console.error(err);
            }
        }).then(() => {
            ctx.status = 200;
            console.log('Movie created');
        });
    })

    .get('/', async ctx => {
        await Movie.find({}, err => {
            if (err) {
                ctx.status = 500;
                console.error(err);
            }
        }).exec().then(movies => {
            ctx.set('Content-Type', 'application/json');
            ctx.status = 200;
            ctx.body = JSON.stringify(movies);
        });
    })

    .get('/:movie', async ctx => {
        let movieID = ctx.params.movie;

        await Movie.findById(movieID, err => {
            if (err) {
                ctx.status = 500;
                console.error(err);
            }
        }).exec().then(movie => {
            ctx.set('Content-Type', 'application/json');
            ctx.status = 200;
            ctx.body = JSON.stringify(movie);
        });
    })

    .delete('/:movie', async ctx => {
        let movieID = ctx.params.movie;

        await Movie.remove({_id: movieID}, err => {
            if (err) {
                ctx.status = 500;
                console.error(err);
            }
        }).exec().then(() => {
            ctx.status = 200;
            console.log('Movie removed');
        });
    })

    .put('/:movie', async ctx => {
        let movieID = ctx.params.movie;
        let body = ctx.request.body;

        await Movie.findById(movieID, err => {
            if (err) {
                ctx.status = 500;
                console.error(err);
            }
        }).exec().then(async movie => {
            movie.title = body.title || movie.title;
            movie.genre = body.genre || movie.genre;
            movie.duration = body.duration || movie.duration;
            movie.MPAA = body.MPAA || movie.MPAA;
            movie.country = body.country || movie.country;

            await movie.save(err => {
                if (err) {
                    ctx.status = 500;
                    console.error(err);
                }
            }).then(() => {
                ctx.status = 200;
                console.log('Movie updated')
            });
        });
    });

sessionsRouter
    .post('/sessions', async ctx => {
        let hall = ctx.params.hall;
        let body = ctx.request.body;

        // TODO #2: Validation!

        await Movie.findById(body.movie, err => {
            if (err) {
                ctx.status = 500;
                console.error(err);
            }
        }).exec().then(async movie => {
            let session = new Session({
                hall: hall,
                time: body.time,
                movie: movie
            });

            await session.save(err => {
                if (err) {
                    ctx.status = 500;
                    console.error(err);
                }
            }).then(() => {
                ctx.status = 200;
                console.log('Session created');
            });
        });
    })

    .get('/sessions', async ctx => {
        let hall = ctx.params.hall;

        await Session.find({hall: hall}, err => {
            if (err) {
                ctx.status = 500;
                console.error(err);
            }
        }).exec().then(sessions => {
            ctx.set('Content-Type', 'application/json');
            ctx.status = 200;
            ctx.body = JSON.stringify(sessions);
        });
    })

    .delete('/sessions/:session', async ctx => {
        let session = ctx.params.session;

        await Session.remove({_id: session}, err => {
            if (err) {
                ctx.status = 500;
                console.error(err);
            }
        }).exec().then(() => {
            ctx.status = 200;
            console.log('Session removed');
        });
    })

    .put('/sessions/:session', async ctx => {
        let sessionID = ctx.params.session;
        let body = ctx.request.body;

        await Session.findById(sessionID, err => {
            if (err) {
                ctx.status = 500;
                console.error(err);
            }
        }).exec().then(async session => {
            session.hall = body.hall || session.hall;
            session.time = body.time || session.time;
            session.movie = body.movie || session.movie;

            await session.save(err => {
                if (err) {
                    ctx.status = 500;
                    console.error(err);
                }
            }).then(() => {
                ctx.status = 200;
                console.log('Session updated');
            });
        })
    });

router.use('/api/movies', moviesRouter.routes()); //, moviesRouter.allowedMethods());
router.use('/api/halls/:hall', sessionsRouter.routes()); //, sessionsRouter.allowedMethods());

module.exports = router